package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.Contacto;
import com.example.pise.objetos.ProcesosPHP;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;

public class TabContacto extends Fragment {

    private EditText txtcorreo;
    private EditText txtcorreoAlterno;
    private EditText txttelefono;
    private EditText txtcelular;
    private ImageView imagenUser;
    private final TabContacto context = this;
    private RequestQueue request2;
    private Button btnmodificar;
    private String IMAGEN="";
    final static String BASE_URL = "https://piseequipo1.000webhostapp.com/WebServices/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_contacto,container, false);
        imagenUser = (ImageView) view.findViewById(R.id.imgPerfil);
        txtcorreo = (EditText) view.findViewById(R.id.txtCorreo);
        txtcorreoAlterno = (EditText) view.findViewById(R.id.txtCorreoAlt);
        txttelefono = (EditText) view.findViewById(R.id.txtTelefono);
        txtcelular  = (EditText) view.findViewById(R.id.txtMovil);
        btnmodificar  = (Button) view.findViewById(R.id.btnModificarContacto);

        final int id = getArguments().getInt("IdAlumno");
        this.request2 = Volley.newRequestQueue(view.getContext());
        this.mostrarAlumnosModificar(id);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ProcesosPHP php = new ProcesosPHP();
        php.setContext(view.getContext());
        btnmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (txtcelular.getText().toString().trim().isEmpty() || txttelefono.getText().toString().trim().isEmpty() || txtcorreoAlterno.getText().toString().trim().isEmpty() || txtcorreo.getText().toString().trim().isEmpty() ) {
                Toast.makeText(getContext(),"Favor de llenar todos los campos y no dejar espacios vacios", Toast.LENGTH_SHORT).show();
            }
            else{
                final int id2 = getArguments().getInt("IdAlumno");
                final Contacto nContacto = new Contacto();
                nContacto.setTelefono(txttelefono.getText().toString());
                Toast.makeText(getContext(),txttelefono.getText().toString(), Toast.LENGTH_SHORT).show();
                nContacto.setMovil(txtcelular.getText().toString());
                nContacto.setCorreo(txtcorreo.getText().toString());
                nContacto.setCorreoAlterno(txtcorreoAlterno.getText().toString());
                Toast.makeText(getContext(), txttelefono.getText().toString(), Toast.LENGTH_SHORT).show();
                php.actualizarContactoWebService(nContacto, id2);
                Toast.makeText(getContext(), "modificado con exito", Toast.LENGTH_SHORT).show();}
            txtcorreoAlterno.setText("");
            txtcorreo.setText("");
            txttelefono.setText("");
            txtcelular.setText("");
            }

        });
    }

    public void mostrarAlumnosModificar(int ID){

        String url = BASE_URL + "contacto.php?" + "&alumno=" + ID;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try {
                JSONObject jsonObject = response.getJSONObject("contacto");
                txttelefono.setText(jsonObject.getString("telefono"));
                txtcelular.setText(jsonObject.getString("celular"));
                txtcorreo.setText(jsonObject.getString("email"));
                txtcorreoAlterno.setText(jsonObject.getString("email_alterno"));
                IMAGEN=jsonObject.getString("foto");
                if(IMAGEN.isEmpty()){
                    imagenUser.setImageResource(R.drawable.usuario);
                }
                else {
                    Picasso.with(getContext()).load(IMAGEN).into(imagenUser);
                }

            } catch (JSONException e){
                e.printStackTrace();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());

                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
        request2.add(request);
    }
}
