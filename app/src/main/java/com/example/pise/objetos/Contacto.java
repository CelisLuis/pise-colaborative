package com.example.pise.objetos;

import java.io.Serializable;

public class Contacto implements Serializable {
    private String telefono;
    private String movil;
    private String correo;
    private String correoAlterno;
    public Contacto() {
    }

    public Contacto(String telefono, String movil, String correo, String correoAlterno) {
        this.telefono = telefono;
        this.movil = movil;
        this.correo = correo;
        this.correoAlterno = correoAlterno;

    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoAlterno() {
        return correoAlterno;
    }

    public void setCorreoAlterno(String correoAlterno) {
        this.correoAlterno = correoAlterno;
    }
}
