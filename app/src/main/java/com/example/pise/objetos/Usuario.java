package com.example.pise.objetos;

public class Usuario {

    private int idUsuario;

    public Usuario(){}

    public Usuario( int idUsuario){
        this.setIdUsuario(idUsuario);

    }


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
