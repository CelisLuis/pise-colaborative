package com.example.pise.objetos;

import java.io.Serializable;

public class Cursos implements Serializable {
    private Integer id;
    private String nombre;
    private String centroInvestigacion;
    private String fecha;

    public Cursos(){
        setId(0);
        setNombre("");
        setCentroInvestigacion("");
        setFecha("");
    }

    public Cursos(Integer id, String nombre, String centro, String fecha){
        this.setId(id);
        this.setNombre(nombre);
        this.setCentroInvestigacion(centro);
        this.setFecha(fecha);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCentroInvestigacion() {
        return centroInvestigacion;
    }

    public void setCentroInvestigacion(String centroInvestigacion) {
        this.centroInvestigacion = centroInvestigacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
