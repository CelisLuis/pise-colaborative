package com.example.pise.objetos;

import java.io.Serializable;

public class Vacante implements Serializable {
    private int id;
    private String nombre;
    private String empresa;
    private String puesto;
    private String fecha_inicio;
    private String fecha_final;
    private String requisitos;

    public Vacante(){
        this.setId(0);
        this.setNombre("");
        this.setEmpresa("");
        this.setPuesto("");
        this.setFecha_inicio("");
        this.setFecha_final("");
        this.setRequisitos("");
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }
}