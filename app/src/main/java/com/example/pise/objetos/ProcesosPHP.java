package com.example.pise.objetos;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    private String serverip = "https://piseequipo1.000webhostapp.com/WebServices/";


    public void login (String user, String password, Context context) {
        String url = serverip + "validar.php?" + "&user=" + user + "&password=" + password;
        request = Volley.newRequestQueue(context);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void mostrarAlumno(int ID,Context con) {

        String url = serverip + "consultarAlumno.php?" + "&idUser=" + ID;
        request = Volley.newRequestQueue(con);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void mostrarDatosModificarPerfil(int ID,Context con) {

        String url = serverip + "consultarAlumno.php?" + "&idUser=" + ID;
        request = Volley.newRequestQueue(con);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void actualizarContactoWebService(Contacto c, int id) {
        String url = serverip + "actualizarContacto.php?id=" + id + "&movil=" + c.getMovil() + "&telefono=" + c.getTelefono() + "&email=" + c.getCorreo() + "&email2=" + c.getCorreoAlterno();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
        @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR -> ", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.i("RESPUESTA -> ", response.toString());
    }


    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

}
