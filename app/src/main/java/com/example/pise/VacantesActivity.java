package com.example.pise;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.Vacante;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VacantesActivity extends FragmentActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Vacante> listaVacante;
    final static String server = "https://piseequipo1.000webhostapp.com/WebServices/";

    private TextView lblNombreVacante;
    private TextView lblEmpresa;
    private TextView lblPuesto;
    private TextView lblEmpresaVac;
    private EditText lblFechaInicio;
    private EditText lblFechaTerminación;
    private TextView lblRequisitos;

    private ListView list;
    private int ID;
    boolean TabVac=false;

    TabHost tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantes);

        lblNombreVacante = (TextView)findViewById(R.id.lblNombreVacante);
        lblEmpresa = (TextView)findViewById(R.id.lblEmpresa);
        list = (ListView)findViewById(android.R.id.list);
        lblPuesto = (TextView)findViewById(R.id.lblPuesto);
        lblEmpresaVac = (TextView)findViewById(R.id.lblEmpresaVac);
        lblFechaInicio = (EditText)findViewById(R.id.lblFechaInicio);
        lblFechaTerminación = (EditText)findViewById(R.id.lblFechaTerminacion);
        lblRequisitos = (TextView)findViewById(R.id.lblRequisitos);

        listaVacante = new ArrayList<Vacante>();
        request = Volley.newRequestQueue(this);
        consultarVacantes();

        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Lista Vacantes");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Vacante");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Vacante vac = null;
        JSONArray json = response.optJSONArray("vacantes");
        try {
            if(TabVac==false)
            {
                for(int i = 0; i < json.length(); i++){
                    vac = new Vacante();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    vac.setId(jsonObject.optInt("id"));
                    vac.setNombre(jsonObject.optString("nombre"));
                    vac.setEmpresa(jsonObject.optString("nombre_empresa"));
                    vac.setFecha_inicio(jsonObject.getString("fecha_inicio"));
                    vac.setFecha_final(jsonObject.getString("fecha_final"));
                    vac.setRequisitos(jsonObject.getString("requisitos"));
                    listaVacante.add(vac);
                }
                MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.activity_vacante, listaVacante);
                list.setAdapter(adapter);

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Vacante vac = listaVacante.get(i);
                        ID = vac.getId();
                        consultarVacante();
                    }
                });

            }else if(TabVac==true){
                JSONObject jsonObject = null;
                for(int i = 0; i < json.length(); i++) {
                    jsonObject = json.getJSONObject(i);
                    lblPuesto.setText(jsonObject.optString("puesto"));
                    lblEmpresaVac.setText(jsonObject.optString("nombre_empresa"));
                    lblFechaInicio.setText(jsonObject.getString("fecha_inicio"));
                    lblFechaTerminación.setText(jsonObject.getString("fecha_final"));
                    lblRequisitos.setText(jsonObject.getString("requisitos"));
                    TabVac=false;
                }
                tabs.setCurrentTab(1);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Vacante> {
        Context context;
        int textViewRecursoId;
        ArrayList<Vacante> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Vacante> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            lblNombreVacante = (TextView)view.findViewById(R.id.lblNombreVacante);
            lblEmpresa = (TextView)view.findViewById(R.id.lblEmpresa);
            list = (ListView)view.findViewById(android.R.id.list);

            lblNombreVacante.setText(objects.get(position).getNombre());
            lblEmpresa.setText(objects.get(position).getEmpresa());

            return view;
        }
    }

    public void consultarVacantes(){
        String url = server + "wsConsultarVacantes.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        TabVac=false;
    }

    public void consultarVacante() {
        String url = server + "wsConsultarVacante.php?idVacante=" + ID;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        TabVac=true;
    }
}
