package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class TabMiPerfil extends Fragment {
    private ImageView imagenLogo;
    private TextView lblNombre2;
    private TextView lblCarrera;
    private TextView lblNacimiento;
    private final TabMiPerfil context = this;
    private RequestQueue request2;
    private TextView lblSexo;
    final static String BASE_URL = "https://piseequipo1.000webhostapp.com/WebServices/";
    String nombreMos="";
    private String IMAGEN="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_mi_perfil,container, false);
        imagenLogo = (ImageView) view.findViewById(R.id.imgPerfil);
        lblNombre2 = (TextView) view.findViewById(R.id.lblNombrePerfil);
        lblCarrera = (TextView) view.findViewById(R.id.lblCarrera);
        lblNacimiento = (TextView) view.findViewById(R.id.lblNacimiento);
        lblSexo = (TextView) view.findViewById(R.id.lblSexo);
        int id = getArguments().getInt("id");
        this.request2 = Volley.newRequestQueue(view.getContext());
        this.mostrarAlumno(id);
        return view;
    }

    public void mostrarAlumno(int ID){

        String url = BASE_URL + "consultarAlumno.php?" + "&idUser=" + ID;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try {
                    JSONObject jsonObject = response.getJSONObject("alumn");
                    String apeP = (jsonObject.getString("p_paterno"));
                    String apeM = (jsonObject.getString("p_materno"));
                    nombreMos = (jsonObject.getString("nombre"));
                    lblNombre2.setText(nombreMos+" "+apeP+" "+apeM);
                    lblCarrera.setText(jsonObject.getString("carrera"));
                    lblNacimiento.setText(jsonObject.getString("fecha_nacimiento"));
                    lblSexo.setText(jsonObject.getString("sexo"));
                    IMAGEN=jsonObject.getString("foto");

                    if(IMAGEN.isEmpty()){
                        imagenLogo.setImageResource(R.drawable.usuario);
                    }
                    else {
                        Picasso.with(getContext()).load(IMAGEN).resize(120, 120).into(imagenLogo);
                    }

            } catch (JSONException e){
                e.printStackTrace();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
        request2.add(request);
    }
}
