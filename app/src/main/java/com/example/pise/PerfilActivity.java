package com.example.pise;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class PerfilActivity extends FragmentActivity  {

    public FragmentTabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        final Integer _ID = extras.getInt("_ID");
        Bundle args = new Bundle();
        args.putInt("id",_ID);

        Intent intent2 = getIntent();
        Bundle extras2 = intent2.getExtras();
        final Integer idAlumno = extras2.getInt("IdAlumno");
        Bundle args2 = new Bundle();
        args2.putInt("IdAlumno",idAlumno);

        tabHost = findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab_mi_perfil").setIndicator("Mi Perfil"), TabMiPerfil.class, args);
        tabHost.addTab(tabHost.newTabSpec("tab_contacto").setIndicator("Contacto"), TabContacto.class, args2);
    }
}