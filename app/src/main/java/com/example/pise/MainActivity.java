package com.example.pise;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.pise.objetos.Usuario;

public class MainActivity extends AppCompatActivity {

    private Button btnPerfil;
    private Button btnActvividadActual;
    private Button btnCursos;
    private Button btnVacantes;
    private final Context context = this;
    private int _ID;
    private int idAlumno;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setTitle("Salir")
            .setMessage("¿Desea salir de la aplicación?")

            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            })

            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPerfil = (Button) findViewById(R.id.btnMiPerfil);
        btnActvividadActual = (Button) findViewById(R.id.btnActvividadActual);
        btnCursos = (Button) findViewById(R.id.btnCursos);
        btnVacantes = (Button) findViewById(R.id.btnVacantes);
        
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        _ID  = extras.getInt("_ID");
        idAlumno = extras.getInt("IdAlumno");

        if ( isNetworkAvailable() ){
            btnPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, PerfilActivity.class);
                    i.putExtra( "_ID", _ID);
                    i.putExtra( "IdAlumno",idAlumno);
                    startActivityForResult(i, 0);

                }
            });

            btnActvividadActual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, ActividadActualActivity.class);
                    i.putExtra( "_ID", _ID);
                    startActivityForResult(i, 0);
                }
            });

            btnCursos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, CursosActivity.class);
                    i.putExtra("_ID", idAlumno);
                    startActivityForResult(i, 0);
                }
            });
            btnVacantes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, VacantesActivity.class);
                    startActivityForResult(i, 0);
                }
            });
        }else {
            Toast.makeText(MainActivity.this, "Necesita conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}
