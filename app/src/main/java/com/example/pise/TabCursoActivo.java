package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.Cursos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TabCursoActivo extends Fragment {
   /* TextView txtNombreCurso, txtImpartido, txtFecha;
    private String serverip = "https://piseequipo1.000webhostapp.com/WebServices/";
    JsonObjectRequest jsonObjectRequest;
    RequestQueue request;

    Cursos curso = new Cursos();
    private Button btnEnviar;
    private RadioButton rdbSi, rdbNo;
    private Integer _ID;
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _ID = Integer.valueOf( getArguments().get("_ID").toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        request = Volley.newRequestQueue(getContext());
        return inflater.inflate(R.layout.tab_curso_activo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtNombreCurso = (TextView)view.findViewById(R.id.txtNombreConferencia);
        txtImpartido = (TextView)view.findViewById(R.id.txtImpartida);
        txtFecha = (TextView)view.findViewById(R.id.txtFechaConferencia);

        txtNombreCurso.setText(curso.getNombre());
        txtImpartido.setText(curso.getCentroInvestigacion());
        txtFecha.setText(curso.getFecha());

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rdbSi.isChecked()){
                    registrarCurso(curso);
                    Toast.makeText(getContext(),"Se ha registrado a este curso", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    protected void displayReciviedData(Cursos curso){
        this.curso.setId(curso.getId());
        consultarCurso(curso);
    }

    public void registrarCurso(Cursos curso){
        String url = serverip + "wsRegistrarCurso.php?idCurso="
                +curso.getId() + "&idUsuario="
                + _ID + "&fecha="
                + curso.getFecha();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void consultarCurso(Cursos curso){
        String url = serverip + "consultarCursosUni.php?idCurso="
                +curso.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(JSONObject response) {
        Cursos curso = null;
        JSONArray json = response.optJSONArray("cursos");
        try {
            for(int i=0;i<json.length();i++){
                curso = new Cursos();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                curso.setId(jsonObject.optInt("idCursos"));
                curso.setNombre(jsonObject.optString("nomCurso"));
                curso.setCentroInvestigacion(jsonObject.optString("nomCentro"));
                curso.setFecha(jsonObject.optString("fechaCurso"));
            }
            this.curso = curso;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/
}
