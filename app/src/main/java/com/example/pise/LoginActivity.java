package com.example.pise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.ProcesosPHP;
import com.example.pise.objetos.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    final static String BASE_URL = "https://piseequipo1.000webhostapp.com/WebServices/";
    private EditText txtUsuario;
    private EditText txtContrasena;
    private ProgressBar spinner;
    private Button btnIngresar;
    private Button btnOlvidaste;
    private final Context context = this;
    private RequestQueue mQueue;

    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnOlvidaste = (Button) findViewById(R.id.btnOlvidaste);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContrasena = (EditText) findViewById(R.id.txtContrasena);

        spinner=(ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);


        mQueue = Volley.newRequestQueue(this);


        if( isNetworkAvailable() ){
            btnIngresar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if( txtUsuario.getText().toString().equals("") || txtContrasena.getText().toString().equals("") ){
                        Toast.makeText(LoginActivity.this, "Ingresa Usuario y/o contraseña", Toast.LENGTH_SHORT).show();
                    }else {
                        login(txtUsuario.getText().toString(), md5(txtContrasena.getText().toString()));
                        spinner.setVisibility(View.VISIBLE);
                    }
                }
            });
            btnOlvidaste.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://siiaa.mx/index.php/login/recuperacion_contrasena"));
                    startActivity(browserIntent);
                }
            });
        }else {
            Toast.makeText(LoginActivity.this, "Requiere acceso a internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void  login( String user, String password){
        String url = BASE_URL + "validar.php?" + "&user=" + user + "&password=" + password;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonObject = response.getJSONObject("usuario");
                    int idUsuario = (Integer.parseInt(jsonObject.getString("idusuario")));
                    Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    int idAlumno = (Integer.parseInt(jsonObject.getString("idAlumno")));
                    mainIntent.putExtra("_ID", idUsuario);
                    mainIntent.putExtra("IdAlumno",idAlumno);
                    LoginActivity.this.startActivity(mainIntent);
                    LoginActivity.this.finish();

                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
                spinner.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Usuario y/o Contraseña incorrecta", Toast.LENGTH_SHORT).show();
            }
        });
        mQueue.add(request);
    }
    public boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private String md5(String in) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(in.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
        return null;
    }
}
