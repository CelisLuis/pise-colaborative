package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;


public class Laboral extends Fragment {

    final static String BASE_URL = "https://piseequipo1.000webhostapp.com/WebServices/";
    private TextView lblUltimoEmpleo;
    private TextView lblNombreLaboral;
    private EditText txtPuesto, txtFechaIngreso, txtEmpresa;
    private Button btnActualizarEmpleado;
    private RequestQueue mQueue;
    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_laboral, container, false);
        lblUltimoEmpleo = ( TextView ) rootView.findViewById(R.id.txtUltimoEmpleo);
        lblNombreLaboral = ( TextView ) rootView.findViewById(R.id.txtNombre);
        btnActualizarEmpleado = ( Button ) rootView.findViewById(R.id.btnActualizarEmpleo);
        txtPuesto = ( EditText ) rootView.findViewById(R.id.txtPuesto);
        txtFechaIngreso = ( EditText ) rootView.findViewById(R.id.txtIngreso);
        txtEmpresa = ( EditText ) rootView.findViewById(R.id.txtEmpresa);
        mQueue = Volley.newRequestQueue(getContext());

        consultarLaboral();
        consultarPerfil();

        btnActualizarEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( !txtEmpresa.getText().toString().equals("") && !txtFechaIngreso.getText().toString().equals("") && !txtPuesto.getText().toString().equals("")){
                    actualizarEmpleo();
                }else {
                    Toast.makeText(getActivity(), "Datos necesarios", Toast.LENGTH_SHORT).show();
                }
                Fragment currentFragment = getFragmentManager().findFragmentByTag("fragment_laboral");
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(currentFragment);
                fragmentTransaction.attach(currentFragment);
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view.getRootView(), savedInstanceState);

    }

    public void consultarLaboral(){
        String url = BASE_URL + "consultarLaboral.php?" + "idUsuario=" + getArguments().getInt("_ID");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try {
                JSONObject jsonObject = response.getJSONObject("laboral");
                lblUltimoEmpleo.setText(jsonObject.getString("nombre_trabajo") + ", " + jsonObject.getString("puesto") + ", " +
                        jsonObject.getString("colonia"));
                Log.i("Respuesta JSON -> ", response.toString());

            } catch (JSONException e){
                e.printStackTrace();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
            }
        });
        mQueue.add(request);
    }

    public void actualizarEmpleo(){
        String puesto = txtPuesto.getText().toString();
        String empresa = txtEmpresa.getText().toString();
        String fecha = txtFechaIngreso.getText().toString();
        String url = BASE_URL + "actualizarLaboral.php?" + "idUsuario=" + getArguments().getInt("_ID") + "&trabajo=" + empresa + "&puesto=" + puesto + "&fecha_inicio=" + fecha;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response-> ", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
            }
        });
        mQueue.add(request);
        txtPuesto.setText("");
        txtEmpresa.setText("");
        txtFechaIngreso.setText("");
        consultarPerfil();
    }
    public void consultarPerfil(){
        String url = BASE_URL + "consultarAlumno.php?" + "idUser=" + getArguments().getInt("_ID");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try {
                JSONObject jsonObject = response.getJSONObject("alumn");
                lblNombreLaboral.setText(jsonObject.getString("nombre"));
                Log.i("Respuesta JSON -> ", jsonObject.toString());

            } catch (JSONException e){
                e.printStackTrace();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
            }
        });
        mQueue.add(request);
    }
}
