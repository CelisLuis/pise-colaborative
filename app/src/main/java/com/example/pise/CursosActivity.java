package com.example.pise;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.Cursos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CursosActivity extends FragmentActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    JsonObjectRequest jsonObjectRequest;
    RequestQueue request;
    private ArrayList<Cursos> listaCursos;
    private Context context = null;
    private String serverip = "https://piseequipo1.000webhostapp.com/WebServices/";

    private TextView txtNombreCurso;
    private TextView txtImpartido;
    private TextView txtFecha;
    private ListView listCurso;
    private RadioButton rdbSi, rdbNo;
    private Button btnEnviar;
    private Integer ID;
    private Integer idAlumno;

    private boolean TabActivo=false;
    Cursos curso = new Cursos();

    TabHost tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursos);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        idAlumno = extras.getInt("_ID");

        txtNombreCurso = (TextView)findViewById(R.id.txtNombreConferencia);
        txtImpartido = (TextView)findViewById(R.id.txtImpartida);
        txtFecha = (TextView)findViewById(R.id.txtFechaConferencia);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        listCurso = (ListView)findViewById(R.id.listCurso);
        rdbSi = (RadioButton)findViewById(R.id.rdbSi);
        rdbNo = (RadioButton)findViewById(R.id.rdbNo);

        listaCursos = new ArrayList<Cursos>();
        request = Volley.newRequestQueue(this);
        consultarCursos();

        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Curso Activo");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Lista Cursos");
        tabs.addTab(spec);

        tabs.setCurrentTab(1);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rdbSi.isChecked()){
                    btnEnviar.setEnabled(false);
                    registrarCurso(curso);
                    Toast.makeText(CursosActivity.this,"Se ha registrado a este curso", Toast.LENGTH_SHORT).show();
                    txtNombreCurso.setText("");
                    txtImpartido.setText("");
                    txtFecha.setText("");
                }
            }
        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Cursos curso = null;
        JSONArray json = response.optJSONArray("cursos");
        try {
            if(TabActivo==false){
                for(int i=0;i<json.length();i++){
                    curso = new Cursos();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    curso.setId(jsonObject.optInt("idCursos"));
                    curso.setNombre(jsonObject.optString("nomCurso"));
                    curso.setCentroInvestigacion(jsonObject.optString("nomCentro"));
                    listaCursos.add(curso);
                }
                MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.view_curso,listaCursos);
                listCurso.setAdapter(adapter);

                listCurso.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Cursos cur = listaCursos.get(i);
                        ID = cur.getId();
                        consultarCurso();
                    }
                });
            }
            else if(TabActivo==true){
                for(int i=0;i<json.length();i++){
                    curso = new Cursos();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    curso.setId(jsonObject.optInt("idCursos"));
                    txtNombreCurso.setText(jsonObject.optString("nomCurso"));
                    txtImpartido.setText(jsonObject.optString("nomCentro"));
                    txtFecha.setText(jsonObject.optString("fechaCurso"));
                    btnEnviar.setEnabled(true);
                    rdbSi.setChecked(true);
                    this.curso.setId(jsonObject.optInt("idCursos"));
                    this.curso.setNombre(jsonObject.optString("nomCurso"));
                    this.curso.setCentroInvestigacion(jsonObject.optString("nomCentro"));
                    this.curso.setFecha(jsonObject.optString("fechaCurso"));
                    tabs.setCurrentTab(0);
                    TabActivo=false;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Cursos> {
        Context context;
        int textViewResourceId;
        ArrayList<Cursos> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Cursos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView lblCurso = (TextView)view.findViewById(R.id.txtCurso);
            TextView lblCentro = (TextView)view.findViewById(R.id.txtCentroInvestigacion);

            lblCurso.setText(objects.get(position).getNombre());
            lblCentro.setText(objects.get(position).getCentroInvestigacion());
            notifyDataSetChanged();
            return view;
        }
    }

    public void consultarCursos(){
        String url = serverip + "wsConsultarCursos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void consultarCurso(){
        String url = serverip + "consultarCursosUni.php?idCurso="
                +ID;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
        TabActivo=true;
    }

    public void registrarCurso(Cursos curso){
        String url = serverip + "wsRegistrarCurso.php?idCurso="
                +curso.getId() + "&idAlumno="
                + this.idAlumno + "&fecha="
                + curso.getFecha();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
}
