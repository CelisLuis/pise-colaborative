package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Posgrado extends Fragment {

    final static String BASE_URL = "https://piseequipo1.000webhostapp.com/WebServices/";

    private View root;
    private EditText txtNombrePosgrado;
    private EditText txtEscuela;
    private TextView txtNombre;
    private RadioGroup rgbDesicion;
    private RadioButton rdbSi;
    private RadioButton rdbNo;
    private Button btnActualizarPosgrado;
    private RequestQueue mQueue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_posgrado, container, false);
        txtEscuela = (EditText) root.findViewById(R.id.txtEscuela);
        txtNombrePosgrado = (EditText) root.findViewById(R.id.txtNombrePosgrado);
        rgbDesicion = (RadioGroup) root.findViewById(R.id.rgbDesicion);
        rdbNo = (RadioButton) root.findViewById(R.id.rdbNo);
        rdbSi = (RadioButton) root.findViewById(R.id.rdbSi);
        btnActualizarPosgrado = (Button) root.findViewById(R.id.btnActualizarPosgrado);
        rdbNo.setChecked(true);
        mQueue = Volley.newRequestQueue(getContext());
        txtNombre = (TextView) root.findViewById(R.id.txtNombre);
        consultarPerfil();

        btnActualizarPosgrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if ( !txtEscuela.getText().toString().equals("") && !txtNombrePosgrado.getText().toString().equals("")){
                int selectedId = rgbDesicion.getCheckedRadioButtonId();
                if ( selectedId == R.id.rdbSi){
                    actualizarPosgrado();
                }else {
                    Toast.makeText(getActivity(), "Debe seleccionar 'Si'", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getActivity(), "Datos necesarios", Toast.LENGTH_SHORT).show();
            }
            }
        });
        actualizarPosgrado();
        return root;
    }

    public void actualizarPosgrado() {
        String url = BASE_URL + "actualizarPosgrado.php?" + "idUsuario=" + getArguments().getInt("_ID")
                + "&nombre=" + txtNombrePosgrado.getText().toString()
                + "&escuela=" + txtEscuela.getText().toString() + "&deseo=" + 1;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response-> ", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
            }
        });
        txtNombrePosgrado.setText("");
        txtEscuela.setText("");
        rdbNo.setChecked(true);
        mQueue.add(request);
    }

    public void consultarPerfil(){
        String url = BASE_URL + "consultarAlumno.php?" + "idUser=" + getArguments().getInt("_ID");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try {
                JSONObject jsonObject = response.getJSONObject("alumn");
                txtNombre.setText(jsonObject.getString("nombre"));

                Log.i("Respuesta JSON -> ", jsonObject.toString());

            } catch (JSONException e){
                e.printStackTrace();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Respuesta error -> ", error.toString());
            }
        });
        mQueue.add(request);
    }
}
