package com.example.pise;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.objetos.Cursos;
import com.example.pise.objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import android.widget.Toast;

public class TabCursos extends ListFragment{
/*
    JsonObjectRequest jsonObjectRequest;
    RequestQueue request;
    private ArrayList<Cursos> listaCursos = new ArrayList<Cursos>();
    private Context context = null;
    private String serverip = "https://piseequipo1.000webhostapp.com/WebServices/";
    SendMessage SM;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        request = Volley.newRequestQueue(context);
        consultarCursos();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_cursos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    interface SendMessage {
        void sendData(Cursos curso);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            SM = (SendMessage) getActivity();
        }catch (ClassCastException e){
            throw new ClassCastException("Error in retrieveng data. PLease try again");
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.i("RESPUESTA -> ", response.toString());
        Cursos curso = null;
        listaCursos.removeAll(listaCursos);
        JSONArray json = response.optJSONArray("cursos");
        try {
            for(int i=0;i<json.length();i++){
                curso = new Cursos();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                curso.setId(jsonObject.optInt("idCursos"));
                curso.setNombre(jsonObject.optString("nomCurso"));
                curso.setCentroInvestigacion(jsonObject.optString("nomCentro"));
                listaCursos.add(curso);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(getContext(),R.layout.view_curso,listaCursos);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    class MyArrayAdapter extends ArrayAdapter<Cursos> {
        Context context;
        int textViewResourceId;
        ArrayList<Cursos> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Cursos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView lblCurso = (TextView)view.findViewById(R.id.txtCurso);
            TextView lblCentro = (TextView)view.findViewById(R.id.txtCentroInvestigacion);
            lblCurso.setText(objects.get(position).getNombre());
            lblCentro.setText(objects.get(position).getCentroInvestigacion());
            notifyDataSetChanged();
            return view;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR -> ", error.toString());
    }

    public void consultarCursos(){
        String url = serverip + "wsConsultarCursos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }*/
}
