package com.example.pise;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class ActividadActualActivity extends FragmentActivity{

    private Intent intent;
    private Bundle bundle;

    public FragmentTabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_actual);

        intent = this.getIntent();
        bundle = intent.getExtras();

        int _ID = bundle.getInt("_ID");

        Bundle argumentos = new Bundle();
        argumentos.putInt("_ID", _ID);

        tabHost= findViewById(android.R.id.tabhost);
        tabHost.setup(this,getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("fragment_laboral").setIndicator("Laboral"),Laboral.class, argumentos);
        tabHost.addTab(tabHost.newTabSpec("fragment_posgrado").setIndicator("Posgrado"),Posgrado.class, argumentos);
    }
}